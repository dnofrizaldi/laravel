@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
 <h3>Buat Account Baru</h3>

    <h5>Sign Up Form</h5>
    <form action="/home" method="post">
        @csrf
        <label for="fname">First name :</label> <br> <br>
        <input type="text" id="fname" name="fname"> <br> <br>
        <label for="lname">Last name :</label> <br> <br>
        <input type="text" id="lname" name="lname"> <br> <br>
        <label>Gender :</label> <br> <br>
        <input type="radio" name="Gender">Male <br>
        <input type="radio" name="Gender">Female <br> <br>
        <label>Nationality </label> <br> <br>
        <select name="" id="">
            <option value="1">Indonesia</option>
            <option value="2">Amerika</option>
            <option value="3">Inggris</option> <br> <br>
        </select> <br> <br>
        <label>Language Spoken</label> <br> <br>
        <input type="checkbox">Bahasa Indonesia <br>
        <input type="checkbox">English <br>
        <input type="checkbox">Other <br> <br>
        <label for="">Bio</label> <br> <br>
        <textarea name="Bio" id="" cols="30" rows="10"></textarea> <br>
        <input type="submit" value="Sign Up">
    </form>
@endsection
   
